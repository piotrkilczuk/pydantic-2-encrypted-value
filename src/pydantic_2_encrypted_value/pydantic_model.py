from typing import Annotated

from cryptography.fernet import Fernet
from pydantic import BaseModel, BeforeValidator, ConfigDict, PlainSerializer

from pydantic_2_encrypted_value.encrypted_value import EncryptedValue


FERNET_KEY = Fernet.generate_key()


def before_validator(value: EncryptedValue | str) -> EncryptedValue:
    if isinstance(value, EncryptedValue):
        return value
    return EncryptedValue(encryption_key=FERNET_KEY, encrypted_b64=value)


def serializer(value: EncryptedValue) -> str:
    return value.encrypted_b64


PydanticEncryptedValue = Annotated[
    EncryptedValue,  # @TODO: should this be an union of EncryptedValue | str | bytes?
    BeforeValidator(before_validator),
    PlainSerializer(serializer, return_type=str),
]


class PydanticModel(BaseModel):
    id: int
    encrypted_value: PydanticEncryptedValue

    model_config = ConfigDict(arbitrary_types_allowed=True)


# Simple instantiation with b64
pm = PydanticModel(
    id=1,
    encrypted_value=EncryptedValue(
        encryption_key=FERNET_KEY, decrypted="test"
    ).encrypted_b64,
)
print("Simple instantiation with b64", pm, pm.encrypted_value.decrypted)

# Simple instantiation with EncryptedValue
pm = PydanticModel(
    id=1, encrypted_value=EncryptedValue(encryption_key=FERNET_KEY, decrypted="test")
)
print("Simple instantiation with EncryptedValue", pm, pm.encrypted_value.decrypted)

# Serialization to JSON
json_str = pm.model_dump_json(indent=2)
print("Serialization to JSON", json_str)

# Serialization to dict
json_dict = pm.model_dump()
print("Serialization to dict", json_dict)
