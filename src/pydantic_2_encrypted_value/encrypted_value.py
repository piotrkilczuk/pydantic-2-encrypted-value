from base64 import b64decode, b64encode
import json
from typing import Union, Any

from cryptography.fernet import Fernet


def symmetric_encrypt(value: Any, key: Union[str, bytes]) -> bytes:
    fernet = Fernet(key)
    serialized_value = json.dumps(value).encode()
    return fernet.encrypt(serialized_value)
 

def symmetric_decrypt(encrypted_value: bytes, key: Union[str, bytes]) -> Any:
    fernet = Fernet(key)
    serialized_value = fernet.decrypt(encrypted_value)
    return json.loads(serialized_value.decode())


def base64decode(value: str) -> str:
    return b64decode(value).decode("utf-8")


def base64encode(value: Union[bytes, str]) -> str:
    value = value.encode("utf-8") if isinstance(value, str) else value
    return b64encode(value).decode("utf-8")


class EncryptedValue:
    """
    This is a wrapper class for values stored in an EncryptedField.

    Any EncryptedField will come back from the database as an EncryptedValue with encrypted bytes inside.
    You can then use .decrypted and .encrypted properties accordingly as you need.

    The decryption will be done lazily when accessing .decrypted, so no decryption is performed when this class
    is instantiated.

    The __repr__ method will also only return a digest of the encrypted value, so it is safe to log your model to
    external systems such as Sentry.
    """

    _encryption_key: Union[str, bytes]
    _encrypted: bytes

    def __init__(
        self,
        encryption_key: Union[str, bytes],
        encrypted: Union[str, bytes, None] = None,
        encrypted_b64: Union[str, None] = None,
        decrypted: Any = None,
    ):
        self._encryption_key = encryption_key
        if not (encrypted is not None) ^ (encrypted_b64 is not None) ^ (decrypted is not None):
            raise ValueError("Specify encrypted, encrypted_b64 or decrypted, but not both!")
        if encrypted_b64:
            encrypted = base64decode(encrypted_b64)
        if encrypted:
            if isinstance(encrypted, str):
                encrypted = encrypted.encode("utf-8")
            self._encrypted = encrypted
        else:
            self._encrypted = self._encrypt(decrypted)

    def __repr__(self):
        digest = self._encrypted.decode()[0:4] + "..." + self._encrypted.decode()[-4:] if self._encrypted else "None"
        return f"<EncryptedValue: {digest}>"

    def __bool__(self):
        return bool(self.decrypted)

    def __len__(self) -> int:
        return len(self.decrypted)

    def _encrypt(self, decrypted: Any) -> bytes:
        return symmetric_encrypt(decrypted, self._encryption_key)

    def _decrypt(self, encrypted: bytes) -> Any:
        return symmetric_decrypt(encrypted, self._encryption_key)

    @property
    def encrypted(self) -> bytes:
        return self._encrypted

    @encrypted.setter
    def encrypted(self, value: bytes):
        self._encrypted = value

    @property
    def encrypted_b64(self) -> str:
        return base64encode(self.encrypted.decode("utf-8"))

    @encrypted_b64.setter
    def encrypted_b64(self, value: str):
        self.encrypted = base64decode(value).encode("utf-8")

    @property
    def decrypted(self) -> Any:
        return self._decrypt(self._encrypted)

    @decrypted.setter
    def decrypted(self, value: Any):
        self._encrypted = self._encrypt(value)
